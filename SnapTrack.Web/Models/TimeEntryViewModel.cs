﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SnapTrack.Entities.Models;
using System.ComponentModel.DataAnnotations;

namespace SnapTrack.Web.Models
{
    public class TimeEntryViewModel
    {
        public int Id { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
        public DateTime DateRecorded { get; set; }
        [Range(0, 24, ErrorMessage = "Time must be between 0 and 24 hours")]
        public double Time { get; set; }
        public WorkItemViewModel WorkItem { get; set; }
        public TimeEntryOption Option { get; set; }

    }
}