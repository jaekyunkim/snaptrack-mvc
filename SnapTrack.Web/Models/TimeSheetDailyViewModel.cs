﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SnapTrack.Web.Models
{
    public class TimeSheetDailyViewModel
    {
        public List<TimeEntryViewModel> TimeEntries { get; set; }
        public DateTime SelectedDate { get; set; }

    }
}