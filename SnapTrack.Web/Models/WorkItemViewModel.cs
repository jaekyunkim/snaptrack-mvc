﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace SnapTrack.Web.Models
{
    public class WorkItemViewModel
    {
        public int Id { get; set; }
        [DisplayName("Task:")]
        [StringLength(20)]
        [Required]
        public string Name { get; set; }
        public ProjectViewModel Project { get; set; }
    }
}