﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace SnapTrack.Web.Models
{
    public class ProjectViewModel
    {
        public int Id { get; set; }
        [DisplayName("Project:")]
        [StringLength(20)]
        [Required]
        public string Name { get; set; }
    }
}
