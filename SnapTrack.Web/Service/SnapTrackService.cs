﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SnapTrack.Web.Models;
using SnapTrack.Entities.Models;
using SnapTrack.Entities.Repositories;
using SnapTrack.Entities.DbContext;
using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace SnapTrack.Web.Service
{
    public class SnapTrackService
    {
        private TimeEntryRepository timeEntryDb;
        private ProjectRepository projectDb;
        private WorkItemRepository workItemDb;
        private SnapTrackDbContext db;

        public SnapTrackService()
        {
            db = new SnapTrackDbContext();
            timeEntryDb = new TimeEntryRepository(db);
            projectDb = new ProjectRepository(db);
            workItemDb = new WorkItemRepository(db);
        } 

        private ProjectViewModel InitializeProjectViewModel(int id, string name)
        {
            return new ProjectViewModel()
            {
                Id = id,
                Name = name
            };
        }

        private WorkItemViewModel InitializeWorkItemViewModel(int id, string name, int projectId, string projectName)
        {
            return new WorkItemViewModel()
            {
                Id = id,
                Name = name,
                Project = InitializeProjectViewModel(projectId, projectName)
            };
        }

        private TimeEntryViewModel InitializeTimeEntryViewModel(int id, DateTime dateRecorded, double time, TimeEntryOption option, int workItemId, string workItemName, int projectId, string projectName)
        {
            return new TimeEntryViewModel()
            {
                Id = id,
                DateRecorded = dateRecorded,
                Time = time,
                Option = option,
                WorkItem = InitializeWorkItemViewModel(workItemId, workItemName, projectId, projectName)
            };
        }

        public List<TimeEntryViewModel> GetTimeEntriesBySelectedDate(DateTime date, string userId)
        {
            //var allTimeEntries = timeEntryDb.GetAll().Where(x => DbFunctions.TruncateTime(x.RecordedDate) == DbFunctions.TruncateTime(date)).ToList<TimeEntry>();
            var allTimeEntries = timeEntryDb.GetAll(userId).Where(x => DbFunctions.TruncateTime(x.RecordedDate) == DbFunctions.TruncateTime(date)).ToList<TimeEntry>();
            return allTimeEntries.Select(
                x => InitializeTimeEntryViewModel(x.Id, x.RecordedDate, x.Time, x.Option, x.WorkItem.Id, x.WorkItem.Name, x.WorkItem.Project.Id, x.WorkItem.Project.Name)).ToList<TimeEntryViewModel>();
            //return this.timeEntryDb.GetAll().Where(x => DbFunctions.TruncateTime(x.RecordedDate) == DbFunctions.TruncateTime(date)).ToList<TimeEntry>().Select(
                //x => GetTimeEntryViewModel(x.Id, x.RecordedDate, x.Time, x.Option, x.WorkItem.Id, x.WorkItem.Name, x.WorkItem.Project.Id, x.WorkItem.Project.Name)).ToList<TimeEntryViewModel>();
        }

        public int CreateProject(string name, string userId)
        {
            var model = db.Projects.Create();
            model.Name = name;
            model.User = db.Users.Find(userId);
            projectDb.Add(model);
            projectDb.Save();
            return model.Id;
        }

        public int CreateWorkItem(string name, string projectId, string userId)
        {
            var model = db.WorkItems.Create();
            model.User = db.Users.Find(userId);
            model.Name = name;
            model.Project = projectDb.GetById(projectId, userId);
            workItemDb.Add(model);
            workItemDb.Save();
            return model.Id;
        }

        public int CreateTimeEntry(DateTime dateRecorded, double time, TimeEntryOption option, string workItemId, string userId)
        {
            var model = db.TimeEntries.Create();
            model.RecordedDate = dateRecorded;
            model.Time = time;
            model.Option = option;
            model.WorkItem = workItemDb.GetById(workItemId, userId);
            timeEntryDb.Add(model);
            timeEntryDb.Save();
            return model.Id;
        }

        public bool DeleteTimeEntry(int id, string userId)
        {
            var success = false;
            if (timeEntryDb.GetById(id.ToString(), userId) != null)
            {
                timeEntryDb.Delete(id.ToString());
                timeEntryDb.Save();
            }
            return success;
        }

        public void SaveTimeEntry(int timeId, double time, TimeEntryOption option, int workItemId, string userId)
        {
            var timeEntry = timeEntryDb.GetById(timeId.ToString(), userId);
            timeEntry.WorkItem = workItemDb.GetById(timeId.ToString(), userId);
            timeEntry.Time = time;
            timeEntry.Option = option;
            timeEntryDb.Update(timeEntry);
            timeEntryDb.Save();
        }

        private List<WorkItem> GetWorkItemsByName(string name, int projectId, string userId)
        {
            return workItemDb.GetAll(userId).Where(x => SqlFunctions.PatIndex("%" + name + "%", x.Name) > 0 && x.Project.Id == projectId).OrderBy(x => x.Name).ToList<WorkItem>();
        }

        public WorkItemViewModel GetWorkItemViewModelByExactName(string name, int projectId, string userId)
        {
            var workItem = workItemDb.GetAll(userId).Where(x => x.Name == name && x.Project.Id == projectId).OrderBy(x => x.Name).ToList<WorkItem>();
            if (workItem.Count != 0)
            {
                return workItem.Select(x => InitializeWorkItemViewModel(x.Id, x.Name, x.Project.Id, x.Project.Name)).FirstOrDefault<WorkItemViewModel>();
            }
            return null;
        }

        public ProjectViewModel GetProjectNameByExactName(string name, string userId)
        {
            var projectItem = projectDb.GetAll(userId).Where(x => x.Name == name).OrderByDescending(x => x.Name).ToList<Project>();
            if (projectItem.Count != 0)
            {
                return projectItem.Select(x => InitializeProjectViewModel(x.Id, x.Name)).FirstOrDefault<ProjectViewModel>();
            }
            return null;
        }

        public List<WorkItemViewModel> AutoCompleteWorkItemViewModel(string term, int projectId, string userId)
        {
            var workItems = GetWorkItemsByName(term, projectId, userId);
            return workItems.Select(x => InitializeWorkItemViewModel(x.Id, x.Name, x.Project.Id, x.Project.Name)).ToList<WorkItemViewModel>();
        }

        public List<ProjectViewModel> AutoCompleteProjectViewModel(string term, string userId)
        {
            var projects = projectDb.GetAll(userId).Where(x => SqlFunctions.PatIndex("%" + term + "%", x.Name) > 0).ToList<Project>();
            return projects.Select(x => InitializeProjectViewModel(x.Id, x.Name)).ToList<ProjectViewModel>();
        }

        public TimeEntryViewModel GetTimeEntryViewModelById(int id, string userId)
        {
            var timeEntry = timeEntryDb.GetById(id.ToString(), userId);
            return InitializeTimeEntryViewModel(timeEntry.Id, timeEntry.RecordedDate, timeEntry.Time, timeEntry.Option, timeEntry.WorkItem.Id, timeEntry.WorkItem.Name, timeEntry.WorkItem.Project.Id, timeEntry.WorkItem.Project.Name);
        }
    }
}