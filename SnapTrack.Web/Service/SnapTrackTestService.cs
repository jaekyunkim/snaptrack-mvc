﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SnapTrack.Web.Models;
using SnapTrack.Entities.Models;

namespace SnapTrack.Web.Service
{
    public static class SnapTrackTestService
    {
        public static TimeEntryViewModel GetTimeEntry()
        {
            ProjectViewModel project = new ProjectViewModel()
            {
                Id = 1,
                Name = "MVC",
            };

            WorkItemViewModel workItem = new WorkItemViewModel()
            {
                Id = 1,
                Name = "Coding",
                Project = project,
            };

            TimeEntryViewModel model = new TimeEntryViewModel()
            {
                Id = 1,
                Time = 4.2,
                WorkItem = workItem,
                Option = TimeEntryOption.TimeBlock
            };

            return model;
        }

        public static List<TimeEntryViewModel> GetMultipleTimeEntries()
        {
            var models = new List<TimeEntryViewModel>();
            var firstModel = GetTimeEntry();
            var secondModel = new TimeEntryViewModel()
            {
                Id = 2,
                Time = 1,
                WorkItem = firstModel.WorkItem,
                Option = TimeEntryOption.Appointment,
            };
            var thirdModel = new TimeEntryViewModel()
            {
                Id = 3,
                Time = 5,
                WorkItem = firstModel.WorkItem,
                Option = TimeEntryOption.Timer,
            };
            models.Add(firstModel);
            models.Add(secondModel);
            models.Add(thirdModel);
            return models;
        }
    }
}