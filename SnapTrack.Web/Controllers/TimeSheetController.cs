﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using SnapTrack.Entities.DbContext;
using SnapTrack.Web.Models;
using SnapTrack.Web.Service;

namespace SnapTrack.Web.Controllers
{
    [Authorize]
    public class TimeSheetController : Controller
    {

        SnapTrackService service = new SnapTrackService();
        // GET: TimeSheet
        public ActionResult Index()
        {
            return RedirectToAction("Daily");
        }

        [HttpPost]
        public ActionResult DailyList(DateTime date)
        {
            return PartialView(service.GetTimeEntriesBySelectedDate(date, User.Identity.GetUserId()));
        }

        public ActionResult Daily()
        {
            //var selectedDate = date ?? DateTime.Now.ToUniversalTime();
            //TimeSheetDailyViewModel model = new TimeSheetDailyViewModel()
            //{
            //    TimeEntries = service.GetTimeEntriesBySelectedDate(selectedDate, User.Identity.GetUserId())
            //};
            return View();
        }
    }
}