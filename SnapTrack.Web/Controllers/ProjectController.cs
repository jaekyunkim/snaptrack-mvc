﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SnapTrack.Web.Models;
using SnapTrack.Web.Service;
using Microsoft.AspNet.Identity;

namespace SnapTrack.Web.Controllers
{
    public class ProjectController : Controller
    {
        SnapTrackService service = new SnapTrackService();

        public JsonResult AutoCompleteProject(string term)
        {
             var result = service.AutoCompleteProjectViewModel(term, User.Identity.GetUserId());
            return new JsonResult() { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}