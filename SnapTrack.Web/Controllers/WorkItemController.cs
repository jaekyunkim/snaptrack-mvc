﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SnapTrack.Web.Models;
using SnapTrack.Web.Service;
using Microsoft.AspNet.Identity;

namespace SnapTrack.Web.Controllers
{
    public class WorkItemController : Controller
    {
        SnapTrackService service = new SnapTrackService();

        public JsonResult AutoCompleteWorkItem(string term, int? projectId)
        {
            int id = projectId ?? 0;
            var result = new List<WorkItemViewModel>();

            if (id != 0)
            {
                result = service.AutoCompleteWorkItemViewModel(term, id, User.Identity.GetUserId());
            }
            return new JsonResult() { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}