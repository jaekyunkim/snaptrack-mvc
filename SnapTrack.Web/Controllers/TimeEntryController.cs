﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SnapTrack.Web.Models;
using SnapTrack.Web.Service;
using Microsoft.AspNet.Identity;
using SnapTrack.Entities.Models;

namespace SnapTrack.Web.Controllers
{
    [Authorize]
    public class TimeEntryController : Controller
    {
        SnapTrackService service = new SnapTrackService();

        [HttpPost]
        public ActionResult Index(TimeEntryViewModel model)
        {
            return PartialView(model);
        }
        
        public ActionResult Edit(int id)
        {
            var model = service.GetTimeEntryViewModelById(id, User.Identity.GetUserId());
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TimeEntryViewModel model)
        {
            if (ModelState.IsValid)
            {
                var projectId = 0;
                var workItemId = 0;
                var project = service.GetProjectNameByExactName(model.WorkItem.Project.Name, User.Identity.GetUserId());
                projectId = project != null ? project.Id : 0;

                if (projectId != 0)
                {
                    var workItem = service.GetWorkItemViewModelByExactName(model.WorkItem.Name, project.Id, User.Identity.GetUserId());
                    workItemId = workItem != null ? workItem.Id : 0;
                }
                else
                {
                    projectId = service.CreateProject(model.WorkItem.Project.Name, User.Identity.GetUserId());
                }

                if (workItemId == 0)
                {
                    workItemId = service.CreateWorkItem(model.WorkItem.Name, projectId.ToString(), User.Identity.GetUserId());
                }
                service.SaveTimeEntry(model.Id, model.Time, model.Option, workItemId, User.Identity.GetUserId());
                return PartialView("Index", model);
            }

            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Approve(int id)
        {
            var model = service.GetTimeEntryViewModelById(id, User.Identity.GetUserId());
            model.Option = TimeEntryOption.TimeBlock;
            service.SaveTimeEntry(model.Id, model.Time, model.Option, model.WorkItem.Id, User.Identity.GetUserId());
            return PartialView("Index", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            service.DeleteTimeEntry(id, User.Identity.GetUserId());
            return new EmptyResult();
        }

        public ActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TimeEntryViewModel model)
        {
            
            if (ModelState.IsValid)
            {
                var projectId = 0;
                var workItemId = 0;
                var project = service.GetProjectNameByExactName(model.WorkItem.Project.Name, User.Identity.GetUserId());
                projectId = project != null ? project.Id : 0;

                if (projectId != 0)
                {

                    var workItem = service.GetWorkItemViewModelByExactName(model.WorkItem.Name, project.Id, User.Identity.GetUserId());
                    workItemId = workItem != null ? workItem.Id : 0;
                }
                else
                {
                    projectId = service.CreateProject(model.WorkItem.Project.Name, User.Identity.GetUserId());
                }

                if (workItemId == 0)
                {
                    workItemId = service.CreateWorkItem(model.WorkItem.Name, projectId.ToString(), User.Identity.GetUserId()); 
                }

                service.CreateTimeEntry(model.DateRecorded, model.Time, model.Option, workItemId.ToString(), User.Identity.GetUserId());

                return PartialView();
            }
            return PartialView(model);
            
        }
    }
}