﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SnapTrack.Web.Startup))]
namespace SnapTrack.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
