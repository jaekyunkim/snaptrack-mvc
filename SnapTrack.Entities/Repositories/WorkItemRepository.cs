﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapTrack.Entities.DbContext;
using SnapTrack.Entities.Models;
using System.Data.Entity;

namespace SnapTrack.Entities.Repositories
{
    public class WorkItemRepository : IRepository<WorkItem>
    {
        private SnapTrackDbContext context;

        public WorkItemRepository(SnapTrackDbContext context)
        {
            this.context = context;
        }

        public IQueryable<WorkItem> GetAll(string userId)
        {
            return context.WorkItems.Where(x => x.Project.User.Id == userId).Include(x => x.Project);
        }

        public WorkItem GetById(string id, string userId)
        {
            int itemId = Convert.ToInt32(id);
            return GetAll(userId).Where(x => x.Id == itemId).ToList().FirstOrDefault<WorkItem>();
        }

        public void Add(WorkItem entity)
        {
            context.WorkItems.Add(entity);
        }

        public void Update(WorkItem entity)
        {
            context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(WorkItem entity)
        {
            context.WorkItems.Remove(entity);
        }

        public void Delete(string id)
        {
            int itemId = Convert.ToInt32(id);
            WorkItem workItem = context.WorkItems.Where(x => x.Id == itemId).ToList().FirstOrDefault<WorkItem>();
            context.WorkItems.Remove(workItem);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
