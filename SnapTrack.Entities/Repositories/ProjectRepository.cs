﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapTrack.Entities.DbContext;
using SnapTrack.Entities.Models;
using System.Data.Entity;

namespace SnapTrack.Entities.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private SnapTrackDbContext context;
        public ProjectRepository(SnapTrackDbContext context)
        {
            this.context = context;
        }

        public void Add(Project entity)
        {
            context.Projects.Add(entity);
        }

        public void Delete(string id)
        {
            int itemId = Convert.ToInt32(id);
            Project project = context.Projects.Where(x => x.Id == itemId).ToList().FirstOrDefault<Project>();
            context.Projects.Remove(project);
        }

        public void Delete(Project entity)
        {
            this.context.Projects.Remove(entity);
        }

        public IQueryable<Project> GetAll(string userId)
        {
            return context.Projects.Where(x => x.User.Id == userId);
        }

        public Project GetById(string id, string userId)
        {
            int itemId = Convert.ToInt32(id);
            return GetAll(userId).Where(x => x.Id == itemId).ToList().FirstOrDefault<Project>();
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Update(Project entity)
        {
            context.Entry(entity).State = EntityState.Modified;
        }
    }
}
