﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapTrack.Entities.DbContext;
using SnapTrack.Entities.Models;
using System.Data.Entity;

namespace SnapTrack.Entities.Repositories
{
    public class TimeEntryRepository : IRepository<TimeEntry>
    {
        private SnapTrackDbContext context;

        public TimeEntryRepository(SnapTrackDbContext context)
        {
            this.context = context;
        }

        public void Add(TimeEntry entity)
        {
            context.TimeEntries.Add(entity);
        }

        public void Delete(string id)
        {
            int itemId = Convert.ToInt32(id);
            TimeEntry timeEntry = context.TimeEntries.Where(x => x.Id == itemId).ToList().FirstOrDefault<TimeEntry>();
            context.TimeEntries.Remove(timeEntry);
        }

        public void Delete(TimeEntry entity)
        {
            this.context.TimeEntries.Remove(entity);
        }

        public IQueryable<TimeEntry> GetAll(string userId)
        {
            return context.TimeEntries.Where(x => x.WorkItem.Project.User.Id == userId).Include(x => x.WorkItem).Include(x => x.WorkItem.Project);
        }

        public TimeEntry GetById(string id, string userId)
        {
            int itemId = Convert.ToInt32(id);
            return GetAll(userId).Where(x => x.Id == itemId).ToList().FirstOrDefault<TimeEntry>();
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Update(TimeEntry entity)
        {
            context.Entry(entity).State = EntityState.Modified;
        }
    }
}
