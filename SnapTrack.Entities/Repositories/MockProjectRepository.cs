﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapTrack.Entities.Models;

namespace SnapTrack.Entities.Repositories
{
    public class MockProjectRepository : IRepository<Project>
    {
        private List<Project> project;
        public MockProjectRepository()
        {
            project = new List<Project>()
            {
                new Project()
            {
                Id = 1,
                Name = "MVC",
            },
        };
        }
        public void Add(Project entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public void Delete(Project entity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Project> GetAll(string userId)
        {
            throw new NotImplementedException();
        }

        public Project GetById(string id, string userId)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Update(Project entity)
        {
            throw new NotImplementedException();
        }
    }
}
