﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnapTrack.Entities.Repositories
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll(string userId);
        T GetById(string id, string userId);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(string id);
        void Save();
    }
}
