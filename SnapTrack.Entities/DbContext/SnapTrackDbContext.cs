﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using SnapTrack.Entities.Models;
using System.Data.Entity;

namespace SnapTrack.Entities.DbContext
{
    public class SnapTrackDbContext : ApplicationDbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<TimeEntry> TimeEntries { get; set; }
        public DbSet<WorkItem> WorkItems { get; set; }

    }
}
