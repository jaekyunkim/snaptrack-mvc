﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using SnapTrack.Entities.Models;
using SnapTrack.Entities.DbContext;

namespace SnapTrack.Entities.Migrations
{
    public class SnapTrackDbInitializer : DropCreateDatabaseAlways<SnapTrackDbContext>
    {

        protected override void Seed(SnapTrackDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Users.AddOrUpdate(p => p.UserName,
                new ApplicationUser
                {
                    Id = "dd6e0719-aea6-4c35-8519-4639c2a025eb",
                    UserName = "kjaek@outlook.com",
                    Email = "kjaek@outlook.com",
                    EmailConfirmed = false,
                    PasswordHash = "AKV/NTxQbvY6biEddO/DNZ11sCAgVCT+LD5z+pg+F+XXY+0NTZW++R9RuOW/9Dm15g==",
                    SecurityStamp = "eb98c581-6b28-4512-ad7e-8521f5068371",
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false,
                    LockoutEnabled = true,
                    AccessFailedCount = 0,
                }
            );
            context.SaveChanges();

            context.Projects.AddOrUpdate(p => p.Name,
                new Project
                {
                    Name = "MVC",
                    User = context.Users.FirstOrDefault(p => p.UserName == "kjaek@outlook.com"),
                });
            context.SaveChanges();

            context.WorkItems.AddOrUpdate(p => p.Name,
                new WorkItem
                {
                    Name = "Coding",
                    User = context.Users.FirstOrDefault(p => p.UserName == "kjaek@outlook.com"),
                    Project = context.Projects.FirstOrDefault(p => p.Name == "MVC"),
                }
            );

            context.SaveChanges();

            context.TimeEntries.AddOrUpdate(p => p.Time,
                new TimeEntry
                {
                    Time = 0,
                    Option = TimeEntryOption.TimeBlock,
                    WorkItem = context.WorkItems.FirstOrDefault(p => p.Name == "Coding"),
                    RecordedDate = DateTime.Now.ToUniversalTime(),
                }
            );
        }
    }
}
