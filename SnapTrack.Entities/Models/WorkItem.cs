﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SnapTrack.Entities.Models
{
    public class WorkItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<TimeEntry> TimeEntries { get; set; }
        public virtual Project Project { get; set; }
        public virtual ApplicationUser User { get; set; }

    }
}
