﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnapTrack.Entities.Models
{
    public enum TimeEntryOption
    {
        TimeBlock = 1, Timer = 2, Appointment = 3
    }
}
