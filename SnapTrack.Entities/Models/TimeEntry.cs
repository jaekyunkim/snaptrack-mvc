﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SnapTrack.Entities.Models
{
    public class TimeEntry
    {
        public int Id { get; set; }
        public double Time { get; set; }
        public virtual WorkItem WorkItem { get; set; }
        [DataType(DataType.Date)]
        public DateTime RecordedDate { get; set; }
        public string Description { get; set; }
        public TimeEntryOption Option { get; set; }

    }
}
